FROM molgenis/maven-jdk17 AS maven
WORKDIR /app
COPY . .
RUN mvn clean install -DskipTests

FROM openjdk:17
WORKDIR /app
#ENV PORT 8080
ENV SPRING_PROFILE production
#ENV DATABASE_URL_TRUE "jdbc:postgresql://hyndqjrxejjgwf:011d26d120414c8135045fd844ebce0661a976a61e4c318d24a0d2e592b87fd8@ec2-52-31-201-170.eu-west-1.compute.amazonaws.com:5432/dbp8uknf4ckfgt"
#DB_URL_FORMAT = jdbc:postgresql://<host>:<port>/<database>?user=<user>&password=<password>
#ENV DATABASE_URL_TRUE "jdbc:postgresql://ec2-54-73-178-126.eu-west-1.compute.amazonaws.com:5432/dc1jfkq7cs5iah?user=gqtmtozexhuryq&password=6568e4606cf5589cea0cc6f0a2eb882e81f1b51d67225ee28cb2d32d631d7023"
ENV DATABASE_URL ""
ENV DDL_AUTO "create"
COPY --from=maven ./app/target/MovieCharacterAPI-0.0.1-SNAPSHOT.jar .
ENTRYPOINT ["java", "-jar", "MovieCharacterAPI-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080

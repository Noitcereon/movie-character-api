package com.experis.moviecharacterapi.Repositories;

import com.experis.moviecharacterapi.Models.Character;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICharacterRepository extends JpaRepository<Character, Integer> {
}

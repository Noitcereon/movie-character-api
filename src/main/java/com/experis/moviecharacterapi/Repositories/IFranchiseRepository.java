package com.experis.moviecharacterapi.Repositories;

import com.experis.moviecharacterapi.Models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IFranchiseRepository extends JpaRepository<Franchise,Integer> {
}

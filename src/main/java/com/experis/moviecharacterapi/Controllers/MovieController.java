package com.experis.moviecharacterapi.Controllers;


import com.experis.moviecharacterapi.Models.Character;
import com.experis.moviecharacterapi.Models.Movie;
import com.experis.moviecharacterapi.Repositories.ICharacterRepository;
import com.experis.moviecharacterapi.Repositories.IMovieRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("api")
public class MovieController {

    private final ICharacterRepository characterRepository;

    private final IMovieRepository movieRepository;

    public MovieController(IMovieRepository movieRepository, ICharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    @GetMapping("movie")
    public ResponseEntity<List<Movie>> getAllMovies() {
        List<Movie> movieData = movieRepository.findAll();
        return ResponseEntity.ok(movieData);
    }

    @GetMapping("movie/{movieId}")
    public ResponseEntity<Movie> findMovieById(@PathVariable Integer movieId) {
        Optional<Movie> searchResult = movieRepository.findById(movieId);
        if (searchResult.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(searchResult.get());
    }

    @GetMapping("movie/{movieId}/characters")
    public ResponseEntity<List<String>> getAllMovieCharacters(@PathVariable Integer movieId){
        List<String> searchResult = movieRepository.findById(movieId).get().getCharacters();
        if(searchResult.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(searchResult);
    }

    @PostMapping("movie")
    public ResponseEntity<Movie> createMovie(@RequestBody Movie movie) {
        Movie createdMovie = movieRepository.save(movie);
        return ResponseEntity.ok(createdMovie);
    }

    @PutMapping("movie/{movieId}")
    public ResponseEntity<Movie> updateMovie(@RequestBody Movie movieUpdate, @PathVariable Integer movieId) {
        if (!movieRepository.existsById(movieId)) {
            return ResponseEntity.notFound().build();
        }
        Movie movie = movieRepository.findById(movieId).get();

        movie.franchise = movieUpdate.franchise;
        movie.director = movieUpdate.director;
        movie.imageUrl = movieUpdate.imageUrl;
        movie.genre = movieUpdate.genre;
        movie.releaseYear = movieUpdate.releaseYear;
        movie.trailerUrl = movieUpdate.trailerUrl;
        movie.title = movieUpdate.title;

        movieRepository.save(movie);
        return ResponseEntity.ok(movie);
    }

    @PatchMapping("movie/{movieId}")
    public ResponseEntity<Movie> setMovieCharacters(@RequestBody Integer[] characterIds, @PathVariable Integer movieId){
        if(!movieRepository.existsById(movieId)){
            return  ResponseEntity.notFound().build();
        }
        Movie movie = movieRepository.findById(movieId).get();
        for (Integer characterId:characterIds) {
            if(!characterRepository.existsById(characterId)){
                return ResponseEntity.notFound().header("ErrorMessage", String.format("Character with id %d not found",characterId)).build();
            }
            if(movie.characters.stream().noneMatch(character -> Objects.equals(character.id, characterId))){
                Character character = new Character();
                character.id = characterId;
                movie.characters.add(character);
            }
        }
        movieRepository.save(movie);
        return ResponseEntity.ok(movie);
    }
    @DeleteMapping("/movie/{movieId}")
    public ResponseEntity<Boolean> deleteMovieById(@PathVariable Integer movieId){
        movieRepository.deleteById(movieId);
        return ResponseEntity.ok(true); // if this is reached it should've been a success
    }

}

package com.experis.moviecharacterapi.Controllers;

import com.experis.moviecharacterapi.Models.Franchise;
import com.experis.moviecharacterapi.Models.Movie;
import com.experis.moviecharacterapi.Repositories.IFranchiseRepository;
import com.experis.moviecharacterapi.Repositories.IMovieRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("api")
public class FranchiseController {

    private final IMovieRepository movieRepository;

    private final IFranchiseRepository franchiseRepository;

    public FranchiseController(IFranchiseRepository franchiseRepository, IMovieRepository movieRepository){this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    @GetMapping("/franchise")
    public ResponseEntity<List<Franchise>> getAllFranchise() {
        List<Franchise> franchiseData = franchiseRepository.findAll();
        return ResponseEntity.ok(franchiseData);
    }

    @GetMapping("/franchise/{franchiseId}")
    public ResponseEntity<Franchise> findFranchiseById(@PathVariable Integer franchiseId){
        Optional<Franchise> searchResult = franchiseRepository.findById(franchiseId);
        if(searchResult.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(searchResult.get());
    }

    @GetMapping("/franchise/{franchiseId}/movies")
    public ResponseEntity<List<String>> getAllFranchiseMovies(@PathVariable Integer franchiseId){
        List<String> searchResult = franchiseRepository.findById(franchiseId).get().getMovies();
        if(searchResult.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(searchResult);
    }

    @GetMapping("/franchise/{franchiseId}/characters")
    public ResponseEntity<List<String>> getAllFranchiseCharacters(@PathVariable Integer franchiseId){
        List<Movie> movies = franchiseRepository.findById(franchiseId).get().movies;
        if(movies.isEmpty()){
            return ResponseEntity.noContent()
                    .header("ErrorMessage", "no movies in franchise")
                    .build();
        }
        List<String> searchResult = new ArrayList<>();
        for (Movie movie: movies) {
            searchResult = Stream.concat(searchResult.stream(),movie.getCharacters().stream())
                    .collect(Collectors.toList());

        }
        if(searchResult.isEmpty()){
            return ResponseEntity.noContent()
                    .header("ErrorMessage", "search result empty")
                    .build();
        }
        return ResponseEntity.ok(searchResult);
    }

    @PostMapping("/franchise")
    public ResponseEntity<Franchise> createFranchise(@RequestBody Franchise franchise) {
        Franchise createdFranchise = franchiseRepository.save(franchise);
        return ResponseEntity.ok(createdFranchise);
    }

    @PutMapping("/franchise/{franchiseId}")
    public ResponseEntity<Franchise> updateFranchise(@RequestBody Franchise franchiseUpdate, @PathVariable Integer franchiseId){
        if(!franchiseRepository.existsById(franchiseId)) {
            return ResponseEntity.notFound().build();
        }
        Franchise franchise = franchiseRepository.findById(franchiseId).get();

        franchise.name = franchiseUpdate.name;
        franchise.description = franchiseUpdate.description;

        franchiseRepository.save(franchise);
        return ResponseEntity.ok(franchise);
    }

    @PatchMapping("franchise/{franchiseId}")
    public ResponseEntity<Franchise> setFranchiseMovies(@RequestBody Integer[] movieIds, @PathVariable Integer franchiseId){
        if(!franchiseRepository.existsById(franchiseId)){
            return ResponseEntity.notFound().build();
        }
        Franchise franchise = franchiseRepository.findById(franchiseId).get();
        for(Integer movieId:movieIds){
            if(!movieRepository.existsById(movieId)){
                return ResponseEntity.notFound().header("ErrorMessage", String.format("Movie with id %d not found",movieId)).build();
            }
            if(franchise.movies.stream().noneMatch(mov -> Objects.equals(mov.id,movieId))){
                Movie movie = movieRepository.findById(movieId).get();
                movie.setFranchise(franchise);
                movieRepository.save(movie);
                franchise.movies.add(movie);
            }
        }
        franchiseRepository.save(franchise);
        return ResponseEntity.ok(franchise);
    }
    @DeleteMapping("/franchise/{franchiseId}")
    public ResponseEntity<Boolean> deleteFranchiseById(@PathVariable Integer franchiseId){
        List<Movie> franchiseMovies = franchiseRepository.findById(franchiseId).get().movies;
        for (Movie movie: franchiseMovies) {
            movie.setFranchise(null);
            movieRepository.save(movie);
        }
        franchiseRepository.deleteById(franchiseId);
        return ResponseEntity.ok(true); // if this is reached it should've been a success
    }

}
